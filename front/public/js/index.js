'use strict';

var stripe = Stripe('STRIPE_API_KEY');

function registerElements(elements, exampleName) {
  var formClass = '.' + exampleName;
  var example = document.querySelector(formClass);

  var soldOut;

  function msToTime( milliseconds ) {
      var day, hour, minute, seconds;
      seconds = Math.floor(milliseconds / 1000);
      minute = Math.floor(seconds / 60);
      seconds = seconds % 60;
      hour = Math.floor(minute / 60);
      minute = minute % 60;
      day = Math.floor(hour / 24);
      hour = hour % 24;
      return {
          day: day,
          hour: hour,
          minute: minute,
          seconds: seconds
      };
  }
  var event = {
    date:null,
    incentiveIncreaseEndDate:null,
    ticketSaleDate:null,
    ticketStartPrice:null,
    ticketEndPrice:null,
    costIncentiveDifference:null,
    maxNumberOfTickets:null,
    incentiveCostPerMilliSecond:null
  }


  $.get( "https://swym.xyz:3000/api/v1/info", function( data ) {
    event = data;
    console.log(event);
      var timeSinceRelease = Date.now() - new Date(event.ticketSaleDate);
      console.log(timeSinceRelease);
      var instanceStartPrice = event.ticketStartPrice + (timeSinceRelease * event.incentiveCostPerMilliSecond)
      console.log("instanceStartPrice " + instanceStartPrice)
      var instanceStartTime = Date.now();


      var interval = setInterval(function(){


        var currentPrice = instanceStartPrice + ((Date.now() - instanceStartTime) * event.incentiveCostPerMilliSecond)
        var nearestMicroPence = Math.round(currentPrice * 1000000) / 1000000
        var nearestpence = Math.round(currentPrice * 100) / 100
        var timeToRelease =  (new Date(event.ticketSaleDate) - Date.now());

          if(Date.now() > new Date(event.ticketSaleDate)){
            if(!soldOut){
              $("#payButton").text("Pay £" + nearestpence);
              $("#dynamicText").text("Dynamic ticket price: £" + nearestMicroPence);
            }else {
              $("#payButton").text("Tickets have sold out!");
              $("#dynamicText").text("");
            }

          }else{
            $("#payButton").text((msToTime(timeToRelease).day) + " Days, " + (msToTime(timeToRelease).hour) + " Hours, " + (msToTime(timeToRelease).minute) + " Minutes, " + (msToTime(timeToRelease).seconds) + " Seconds");
            $("#dynamicText").text("");
          }

      },100);

    });

  $.get( "https://swym.xyz:3000/api/v1/ticket", function( data ) {
    soldOut = data;
  });






  var form = example.querySelector('form');
  var resetButton = example.querySelector('a.reset');
  var error = form.querySelector('.error');
  var errorMessage = error.querySelector('.message');

  function enableInputs() {
    Array.prototype.forEach.call(
      form.querySelectorAll(
        "input[type='text'], input[type='email'], input[type='tel']"
      ),
      function(input) {
        input.removeAttribute('disabled');
      }
    );
  }

  function disableInputs() {
    Array.prototype.forEach.call(
      form.querySelectorAll(
        "input[type='text'], input[type='email'], input[type='tel']"
      ),
      function(input) {
        input.setAttribute('disabled', 'true');
      }
    );
  }

  function triggerBrowserValidation() {
    // The only way to trigger HTML5 form validation UI is to fake a user submit
    // event.
    var submit = document.createElement('input');
    submit.type = 'submit';
    submit.style.display = 'none';
    form.appendChild(submit);
    submit.click();
    submit.remove();
  }

  // Listen for errors from each Element, and show error messages in the UI.
  var savedErrors = {};
  elements.forEach(function(element, idx) {
    element.on('change', function(event) {
      if (event.error) {
        error.classList.add('visible');
        savedErrors[idx] = event.error.message;
        errorMessage.innerText = event.error.message;
      } else {
        savedErrors[idx] = null;

        // Loop over the saved errors and find the first one, if any.
        var nextError = Object.keys(savedErrors)
          .sort()
          .reduce(function(maybeFoundError, key) {
            return maybeFoundError || savedErrors[key];
          }, null);

        if (nextError) {
          // Now that they've fixed the current error, show another one.
          errorMessage.innerText = nextError;
        } else {
          // The user fixed the last error; no more errors.
          error.classList.remove('visible');
        }
      }
    });
  });

  // Listen on the form's 'submit' handler...
  form.addEventListener('submit', function(e) {
    e.preventDefault();


    if(Date.now() > (new Date(event.ticketSaleDate))){


    // Trigger HTML5 validation UI on the form if any of the inputs fail
    // validation.
    var plainInputsValid = true;
    Array.prototype.forEach.call(form.querySelectorAll('input'), function(
      input
    ) {
      if (input.checkValidity && !input.checkValidity()) {
        plainInputsValid = false;
        return;
      }
    });
    if (!plainInputsValid) {
      triggerBrowserValidation();
      return;
    }

    // Show a loading screen...
    example.classList.add('submitting');

    // Disable all inputs.
    disableInputs();

    // Gather additional customer data we may have collected in our form.
    var name = form.querySelector('#' + exampleName + '-name');
    var email = form.querySelector('#' + exampleName + '-email');
    var phone = form.querySelector('#' + exampleName + '-phone');
    var city = form.querySelector('#' + exampleName + '-city');
    var state = form.querySelector('#' + exampleName + '-state');
    var zip = form.querySelector('#' + exampleName + '-zip');
    var additionalData = {
      name: name ? name.value : undefined,
      email: email ? email.value : undefined,
      phone: phone ? phone.value : undefined,
    };

    // Use Stripe.js to create a token. We only need to pass in one Element
    // from the Element group in order to create a token. We can also pass
    // in the additional customer data we collected in our form.
    stripe.createToken(elements[0], additionalData).then(function(result) {
      // Stop loading!


      if (result.token) {
        console.log(additionalData.name);
        // If we received a token, show the token ID.

        $.ajax({
            type: "POST",
            url: "https://swym.xyz:3000/api/v1/ticket",
            data: {name: additionalData.name,email: additionalData.email,phone:additionalData.phone, stripeToken:result.token.id},
            success: function(data){
              console.log(data);
                example.classList.remove('submitting');
              if(data.success){
                  swal(data.title, "🌊 Your going swymming!" + " Ref: " + data.data.ref + " 🌊", "success");
              } else{
                swal("Payment Error", data.message, "error");
                if(data.soldOut){
                  soldOut = true;
                }
              }
              form.reset();
              enableInputs();
              elements.forEach(function(element) {
                element.clear();
              });
            },
            error: function(res) {
              example.classList.remove('submitting');

              enableInputs();

              swal("Connection Error", "Something went wrong our end, you were not charged.", "error");

            }
          });






      } else {
        // Otherwise, un-disable inputs.
        enableInputs();
      }
    });
  } else {
    console.log(new Date(event.ticketSaleDate));
      swal("🐎 Hold your horses 🐎", "Tickets aren't out yet!", "error");
  }
  });



  resetButton.addEventListener('click', function(e) {
    e.preventDefault();
    // Resetting the form (instead of setting the value to `''` for each input)
    // helps us clear webkit autofill styles.
    form.reset();

    // Clear each Element.
    elements.forEach(function(element) {
      element.clear();
    });

    // Reset error state as well.
    error.classList.remove('visible');

    // Resetting the form does not un-disable inputs, so we need to do it separately:
    enableInputs();
    example.classList.remove('submitted');
  });
}
