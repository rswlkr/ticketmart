

let router = require('express').Router();

// Set default API response
router.get('/', function (req, res) {
    res.json({
       status: 'API is alive',
       message: 'Woot',
    });
});

// Import ticket controller
var ticketController = require('./controllers/tickets/ticketController');

// Tickets
router.route('/ticket')
    .get(ticketController.checkSoldOut)
    .post(ticketController.index)

router.route('/info')
    .get(ticketController.info)

// Export API routes
module.exports = router;
