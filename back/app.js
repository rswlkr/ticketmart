var express = require('express');
var app = express();
var bodyParser = require('body-parser');
let mongoose = require('mongoose');
var cors = require('cors');
var fs = require('fs')
var https = require('https')

var port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/swym',{user:'********',pass:'*******'});
var db = mongoose.connection;


app.use(cors({origin: '*'}));

let routes = require("./routes")


app.use(express.static('public'))
app.use('/api/v1', routes)


https.createServer({
  key: fs.readFileSync('privkey.pem'),
  cert: fs.readFileSync('cert.pem')
}, app)
.listen(3000, function () {
  console.log('App listening on port 3000! Go to https://localhost:3000/')
})
