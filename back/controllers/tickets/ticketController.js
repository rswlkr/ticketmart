// propertyController.js
// Import property model
let Ticket = require('../../models/ticketModel')
var QRCode = require('qrcode')
var stripe = require("stripe")("STRIPE_API_KEY");
const shortid = require('shortid');
const nodemailer = require("nodemailer");
var ticketsSold;
var ticketsReserved;

var event = {
  date:null,
  incentiveIncreaseEndDate:null,
  ticketSaleDate:null,
  ticketStartPrice:null,
  ticketEndPrice:null,
  costIncentiveDifference:null,
  maxNumberOfTickets:null,
  incentiveCostPerMilliSecond:null
}

event.date = new Date("2019","02","23","20","30"),
event.incentiveIncreaseEndDate = new Date("2019","02","06","17","00")
event.ticketSaleDate = new Date("2019","01","27","17","00")
event.ticketStartPrice = 15;
event.ticketEndPrice = 18;
event.costIncentiveDifference = event.ticketEndPrice - event.ticketStartPrice;
event.maxNumberOfTickets = 250;
event.incentiveCostPerMilliSecond = event.costIncentiveDifference / (event.incentiveIncreaseEndDate - event.ticketSaleDate),

console.log(event);


Ticket.count({eventId: "1"}, function(err, c) {
      ticketsSold = c;
      ticketsReserved = c;

});


// async..await is not allowed in global scope, must use a wrapper
async function sendTicket(to, eventid, ref){


  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.sendgrid.net",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'apikey', // generated ethereal user
      pass: 'SENDGRID_API_KEY' // generated ethereal password
    }
  });

  // setup email data with unicode symbols
  let mailOptions = {
    from: '"SWYM" <tickets@swym.xyz>', // sender address
    to: to, // list of receivers
    subject: "🎫 Ticket 🎫", // Subject line
    html: "<div>Ticket Reference: " + ref + "<div><img src='https://swym.xyz:3000/tickets/" + ref + ".png' /></div>"
  };

  // send mail with defined transport object
  let info = await transporter.sendMail(mailOptions)

  console.log("Message sent: %s", info.messageId);
  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

// Handle create ticket actions
exports.index = function (req, res) {
  ticketsReserved++;
  console.log(req.body);
console.log(ticketsSold);
if(Date.now() > event.ticketSaleDate){
      //Check to see if tickets have sold out
      if(ticketsReserved <= event.maxNumberOfTickets && ticketsSold < event.maxNumberOfTickets) {

      var timeSinceRelease = Date.now() - event.ticketSaleDate;


      let costToChargePence = 0;



      console.log("Cost to charge: " +  costToChargePence);

    var ticket = new Ticket();
    ticket.ref = shortid.generate();
    ticket.eventId = "1";
    ticket.name = req.body.name;
    ticket.email = req.body.email;
    ticket.phone = req.body.phone;

    const token = req.body.stripeToken; //  Using Express
    console.log(req.body.stripeToken);
    //If tickets sold is greater than two, issue paid tickets.
    if(ticketsReserved > 2){
      var costToCharge = event.ticketStartPrice + (timeSinceRelease * event.incentiveCostPerMilliSecond)
      costToChargePence = Math.round(costToCharge * 100)

    stripe.charges.create({
      amount: costToChargePence,
      currency: "gbp",
      source: token, // obtained with Stripe.js
      description: "SWYM Ticket Ref: " + ticket.ref,
      metadata: {ticketRef: ticket.ref},
      receipt_email: ticket.email
    }, function(err, charge) {
      // asynchronously called
      if(!err) {
        ticketsSold++;
        ticket.save(function (err) {
          if(!err) {

            QRCode.toFile("public/tickets/" + ticket.ref + ".png", ticket.ref , {
              scale: 10,
            }, function (err) {
              if (err) throw err
              sendTicket(ticket.email, ticket.eventId,ticket.ref).catch(console.error);

            })

            res.json({
                title: 'Payment Complete',
                message: "🌊 You're going swyming! 🌊",
                success: true,
                data: ticket
            });
          }else{
            ticketsReserved--;
            res.send(err);
          }

        });

      }

    });

    //ticket count with current event ID is less than or = 2 so issue free ticket.
  } else{
ticketsSold++;
ticket.free = true;
 ticket.save(function (err) {
      if(!err) {

        QRCode.toFile("public/tickets/" + ticket.ref + ".png", ticket.ref , {
          scale: 10,
        }, function (err) {
          if (err) throw err
          sendTicket(ticket.email, ticket.eventId,ticket.ref).catch(console.error);

        })

        res.json({
            title: '🎉 FREE TICKET! 🎉',
            message: "🌊 You're going swyming! 🌊",
            success: true,
            data: ticket
        });
      }else{
        ticketsReserved--;
        res.send(err);
      }

    });
}

  } else {
    ticketsReserved--;
    res.json({
      success: false,
      message: "😢 tickets have sold out 😢",
      soldOut: true,
    })
  }


} else {
  ticketsReserved--;
  res.json({
    success: false,
    message: "Tickets are not out yet!"
  })
}



};

exports.info = function (req, res) {
  	res.send(event);

};

exports.checkSoldOut = function (req, res) {

  Ticket.count({eventId: "1"}, function(err, c) {
    if(!err){
	console.log(event.maxNumberOfTickets);
      if(c < event.maxNumberOfTickets) {
        res.send(false);
  } else {
    res.send(true)
  }


}
});


};

  // Property.find(req.params.property_id, function (err, property) {
  //         if (err)
  //             res.send(err);
  //             property.name = req.body.name ? req.body.name : property.name;
  //
  // // save the property and check for errors
  //         property.save(function (err) {
  //             if (err)
  //                 res.json(err);
  //             res.json({
  //                 message: 'Property Info updated',
  //                 data: property
  //             });
  //         });
  //     });
