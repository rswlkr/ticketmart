// propertyModel.js
var mongoose = require('mongoose');
// Setup schema
var ticketSchema = mongoose.Schema({
  dateCreated: {
      type: Date,
      default: Date.now
  },
    name: {
        type: String,
        required: true
    },
    email: {
      type: String,
      default: true
    },
    phone: {
      type:String,
      required: true
    },
    eventId:{
      type:String,
      required: true
    },
    ref: {
      type: String,
       required: true
    },
    scanned: {
      type: Boolean,
      default: false,
    },
	free:{
	type:Boolean,
	default: false,
},


});
// Export Property model
var Ticket = module.exports = mongoose.model('ticket', ticketSchema);
// module.exports.get = function (callback, limit) {
//     Property.find(callback).limit(limit);
// }
